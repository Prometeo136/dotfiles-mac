# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
# export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# ZSH_THEME="sunrise"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
# plugins=(git)

# source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
# export EDITOR='vim'
# else
# export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#


#############################  Antigen #############################
# source $HOME/.antigen.zsh
# antigen bundle docker
# antigen bundle zsh-users/zsh-syntax-highlighting
# antigen bundle zsh-users/zsh-completions
# antigen bundle zsh-users/zsh-autosuggestions
# antigen bundle wfxr/forgit
# # Tell Antigen that you're done.
# antigen apply
####################################################################


################################## PLUGINS ##################################
autoload -Uz compinit
compinit

[ -s "/Users/prometeo/.scm_breeze/scm_breeze.sh" ] && source "/Users/prometeo/.scm_breeze/scm_breeze.sh"
source $HOME/dotfiles/zshrc/modules/forgit/forgit.plugin.zsh
source $HOME/dotfiles/zshrc/modules/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
source $HOME/dotfiles/zshrc/modules/zsh-completions/zsh-completions.plugin.zsh
source $HOME/dotfiles/zshrc/modules/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOME/dotfiles/zshrc/modules/aliases/aliases.plugin.zsh

#############################################################################


##################################  Rust  ##################################
export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"
###########################################################################

######################## Import aliases from dotfile ########################
source ~/.aliases
#############################################################################

############################# Functions #############################
# Create a new directory and enter it
function mk() {
  mkdir -p "$@" && cd "$@"
}
#####################################################################

#############################  PTAHS #############################
export PATH="$HOME/.local/bin/:$PATH"
##################################################################

############################# PYTHON #############################

# PYENV
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
# export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv init --path)"

eval "$(pyenv virtualenv-init -)"
export PYENV_VIRTUALENV_DISABLE_PROMPT=1

##################################################################

########################### Starship ###########################
# starship:  https://starship.rs/
eval "$(starship init zsh)"
################################################################

########################### Brezee ###########################
eval [ -s "/home/prometeo/.scm_breeze/scm_breeze.sh" ] && source "/home/prometeo/.scm_breeze/scm_breeze.sh"
#############################################################

##################### Import env variables #####################
# source $HOME/dotfiles/env_vars/variables

#################### Enable ctrl arrows ####################
bindkey -e
bindkey ";5C" forward-word
bindkey ";5D" backward-word

################ Case insensitive completions ################
autoload -Uz compinit && compinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
